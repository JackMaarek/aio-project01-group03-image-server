<?php

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * UserController constructor.
     *
     * @param UserService         $userService
     * @param TranslatorInterface $translator
     */
    public function __construct(
        UserService $userService,
        TranslatorInterface $translator
    ) {
        $this->userService = $userService;
        $this->translator = $translator;
    }

    /**
     * Register a user.
     *
     * @Route("/registration", name="registration", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $jsonRequest = json_decode($request->getContent(), true);
        $user = $this->userService->createUser($jsonRequest);
        if (null === $user) {
            return new JsonResponse([
                'errors' => [
                    $this->translator->trans('user.not_created', [], 'user'),
                ],
            ],
                JsonResponse::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        return new JsonResponse([
            $this->translator->trans('user.created', [], 'user'),
        ],
            JsonResponse::HTTP_CREATED
        );
    }
}
