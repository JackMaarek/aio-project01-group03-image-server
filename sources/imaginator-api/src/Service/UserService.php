<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface       $entityManager
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->entityManager = $entityManager;
        $this->encoder = $encoder;
    }

    /**
     * Creates a User.
     *
     * @param array $jsonRequest
     *
     * @return user|null
     */
    public function createUser(
        array $jsonRequest
    ): ?User {
        if (!\array_key_exists('email', $jsonRequest) && !\array_key_exists('password', $jsonRequest)) {
            return null;
        }
        /** @var User $user */
        $user = new User();
        $user->setPassword($this->encoder->encodePassword($user, $jsonRequest['password']));
        $user->setEmail($jsonRequest['email']);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
