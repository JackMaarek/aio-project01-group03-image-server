# base image
FROM node:12.2.0-alpine

# set working directory
WORKDIR /imaginator-app

# add `/app/node_modules/.bin` to $PATH
ENV PATH ./sources/imaginator-app/node_modules/.bin:$PATH

ADD ./sources/imaginator-app/package.json .
ADD ./sources/imaginator-app/package-lock.json .

# install and cache app dependencies
RUN npm install
RUN npm install -g @vue/cli

# start app
CMD ["npm", "run", "serve"]
