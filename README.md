# Imaginator

A small aio application that can handle image resizing, croping, and filtering. Can offer multiple plans to get more features like content management and branding management.

## Requirements

## Project setup


## Integration setup


## Development process

Here is a quick explanation of the development process:

1. Most of the time assign a ticket on the [Application board](https://gitlab.com/JackMaarek/aio-project01-group03-image-server/-/boards/1760656) to you. If it is not the case and you are told to do it yourself, assign the gitlab issue to you. 
2. When you start working on the ticket, move the concerned ticket to `In Progress`.
3. Create a branch specifically for this ticket with a name that follows the [conventions specified below](#branch-naming-convention).
4. Commit regularly at each significant step with unambiguous commit messages (see [COMMIT_CONVENTIONS](COMMIT_CONVENTIONS.md) file).
5. Create a merge request that follows the [conventions specified below](#merge-requests-mr) to the develop branch.
6. On the [Application board](https://gitlab.com/JackMaarek/aio-project01-group03-image-server/-/boards/1760656), move the ticket to the status `In Review`
7. Assign the merge request to a maintainer
8. # Your pull request will then be merged into the develop branch and the concerned ticket will be moved to `Done`

## Merge requests (MR)

To create a merge requests in this project, you will need to use the templates available in the [MERGE_REQUEST_TEMPLATE](.gitlab/merge_request_templates/MERGE_REQUEST.md):

If your merge request is still work in progress, please add "WIP: " (Work In Progress) in front of the title, therefor you inform the maintainers that your work is not done, and we can't merge it.

The naming of the MR should follow the same rules as the [COMMIT_CONVENTIONS](COMMIT_CONVENTIONS.md) and link the concerned issue (feature/bug).

## Branch naming convention

The branch should have a name that reflects it's purpose.

It should use the same guidelines as [COMMIT_CONVENTIONS](COMMIT_CONVENTIONS.md) (`feat`, `fix`, `build`, `perf`, `docs`), followed by an underscore (`_`) and a very quick summary of the subject in [kebab case][1].

Example: `feat_add-image-tag-database-relation`.

## Git hooks


## Linter

## Continuous Integration (CI)

A CI pipeline is configured for this project and is accessible in the [.gitlab-ci.yaml](.gitlab-ci.yaml) file.

The pipeline will run 3 different jobs: 
- Dependencies check
- Linter
- Tests

The pipeline will be triggered automatically when creating a new Pull Request and on each push on it.
It will also be triggered on push on `master`and `dev` branches.


## License

[License](LICENSE.md)

[1]: https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841